![license](https://img.shields.io/github/license/mashape/apistatus.svg)
![py36 status](https://img.shields.io/badge/python3.6-supported-green.svg)

<img src="media/encore_logo.png" alt="encore_logo" width="400"/>

# ENtorhinal COmpression REconstruction: a self-supervised model of the hippocampus

ENCORE is a simple, ready-to-use, self-supervised network implemented in Keras to test rate-based benchmarks of the hippocampal formation.
ENCORE was tested in the spatial domain, thus learning spatial representations (i.e. place-cells), detecting environmental modifications and novelty, performing rate remapping and firing-field expansion. 
Refer to the original paper for further details https://www.cell.com/iscience/fulltext/S2589-0042(21)00332-1. 

<img src="media/encore_model_benchmarks.png" alt="encore_model_benchmarks" width="800"/>


## Example Usage
```
from encore import Encore
from env import Arena

n_mec = 90
n_lec = 210

arena = Arena(arena_size=[60,60], n_mec=n_mec, n_lec=n_lec)
data = arena.get_rateMaps()

## Define parameters of the model.
n_DG, n_CA3, n_CA1 = [1000, 200, 300] 

model = Encore(n_DG=n_DG, n_CA3=n_CA3, n_CA1=n_CA1, dim=n_mec+n_lec)
history = model.train(data=data, epochs=200)
```

## Try it out
We invite you to play around with ENCORE directly in your browser, and test further hippocampal features.
Launch an ipython environment:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/diogo.santos.pata%2Fencore/master)

Once inside, open the `main.ipynb` notebook and have fun!

## The theory behind
We are releasing an opinion article (in press) with the details of the ingredients for self-supervised learning in the hippocampus

Stay tuned!




