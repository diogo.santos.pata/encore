import numpy as np
import tensorflow as tf
from tensorflow.keras.models import Model, load_model
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense, Input
from tensorflow.keras.optimizers import RMSprop


class Encore(object):
    
    def __init__(self, n_DG=100, n_CA3=80, n_CA1=100, dim=300, summary=True):
        '''
        Initializes an instance of ENCORE (ENtorhinal COmpression-REconstruction hippocampal model). We use keras to build an autoencoder-like network.
        Params:
            n_* (int) : Number of units per each layer (Dentate Gyrus or DG, CA3, and CA1).
            dim (int) : Input/output dimension (1-D in our case).
            summary (bool) : If True, it outputs the keras summary of the model.
        '''
        
        self.n_DG = n_DG
        self.n_CA3 = n_CA3
        self.n_CA1 = n_CA1
        self.EC_dim = dim
        
        self.model = self.create_model(summary)

        
    def create_model(self, summary):

        model = Sequential([
            Input(shape=(self.EC_dim,), name='EC_superficial'),
            Dense(self.n_DG, activation='relu', name='DG'),
            Dense(self.n_CA3, activation='relu', name='CA3'),
            Dense(self.n_CA1, activation='relu', name='CA1'),
            Dense(self.EC_dim, activation='relu', name='EC_deep')
        ])

        optimizer = RMSprop(lr=0.001)
        model.compile(loss='mse', optimizer=optimizer, metrics=['mae'])
        
        if summary:
            model.summary()

        return model
    
    
    def save_model(self, name='ENCORE'):
        
        self.model.save(name+'.h5')
    
    
    def load_model(self, name='ENCORE'):
        
        self.model = load_model(name+'.h5')

        
    def train(self, data, epochs=100):

        idx = np.arange(data.shape[0])
        np.random.shuffle(idx)

        history = self.model.fit(data[idx], data[idx], epochs=epochs, validation_split=0.2, verbose=False)

        return history
    
    
    def test(self, data):
        
        return self.model.predict(data)
    
    
    def get_output(self, layer, data):
        
        model = Model(inputs=self.model.inputs, outputs=self.model.get_layer(layer).output)

        return model.predict(data)
