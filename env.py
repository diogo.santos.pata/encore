import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage.filters import gaussian_filter


class Arena(object):
    
    def __init__(self, arena_size=[50,50], n_mec=90, n_lec=210):
        '''
        Initializes the simulated arena characterized by the LEC and MEC ratemaps to be used as population vectors to train the ENCORE model.
        '''
        
        self.dims = arena_size
        self.n_mec = n_mec
        self.n_lec = n_lec
        self.rateMaps = self.create_rateMaps()
        
        
    def create_rateMaps(self):

        mec_rateMap = []
        for ii in range(self.n_mec):
            lamb = np.random.randint(500, 2000)
            phase = np.random.randint(0, self.dims[0], 2)
            m = self.MEC_rateMap(phase=phase, lamb=lamb)
            mec_rateMap.append(m.flatten())
        mec_rateMap = np.array(mec_rateMap)

        lec_rateMap = []
        for ii in range(self.n_lec):
            l = self.LEC_rateMap(filled_perc=0.2)
            lec_rateMap.append(l.flatten())
        lec_rateMap = np.array(lec_rateMap)

        rateMaps = np.vstack((mec_rateMap, lec_rateMap)).T

        return rateMaps
        
        
    def MEC_rateMap(self, theta=0., phase=[50, 50], lamb=500):
        '''
        Grid cells activity maps as presented in:
        - Blair et al. (2007), equation (1).
        - Almeida et al. (2009), equation (1).
        Params:
            arena_size (tuple of int) : define rate_map dimensions
            theta (float)             : Grid rotation (assume to be either 0°, 20°, or 40°, in degrees)
            Phase (tuple of int)      : Spatial phase of the grid     
            lamb (int)                : Distance between firing fields
        '''

        M = np.zeros(self.dims)
        a = 0.3
        b = -3. / 2.
        lambV = (4 * np.pi) / (np.sqrt(3 * lamb))
        theta = np.radians(theta)

        for ind, val in np.ndenumerate(M):

            tmp_m = 0
            for i in np.deg2rad(np.linspace(-30, 90, 3)):
                u_f = (np.cos(i + theta), np.sin(i + theta))
                dist = (ind[0] - phase[0], ind[1] - phase[1])
                tmp_m += np.cos(lambV * np.dot(u_f, dist))

            tmp_m = np.exp(np.dot(a, (tmp_m) + b)) - 1
            M[ind] = tmp_m

        if M.min() < 0: 
            M += abs(M.min())
            
        M = (M - M.min()) / (M.max() - M.min())

        return M


    def LEC_rateMap(self, filled_perc=0.2):
        '''
        Build LEC cell-like rate maps. See Renno-Costa et al. 2010.
        These cells have X (filled_perc) number of firing fields randomly spread over the arena.
        Params:
            arena_size (tuple of int) : define rate_map dimensions
            filled_perc (float)       : define how much area this cell is receptive to (value from 0 to 1).
        '''

        a = np.zeros(36)
        a[: int(filled_perc * 25)] = 1
        np.random.shuffle(a)
        a = a.reshape(6, 6)

        b = np.zeros(self.dims)

        for i in range(self.dims[0]):
            for j in range(self.dims[1]):
                idx1 = i * len(a) // self.dims[0]
                idx2 = j * len(a) // self.dims[1]
                b[i][j] = a[idx1][idx2]

        arena = gaussian_filter(b, 4)
        arena *= 0.6

        return arena

    
    # dd goes from 0 to 1 and it's the extent by which the current LEC rate maps are substituted by new ones.
    def modify_LEC_maps(self, dd, permanent=False):
        '''
        Modifies LEC maps by substituting a random proportion ("dd") of them by novel ones.
        Params:
            dd (float) : Proportion of ratemaps to be modified w.r.t. initial arena. Value goes from 0 to 1.
            permanent (bool) : Makes the changes permanent to the arena instance. If false, it simply returns the new ratemaps.
        '''
        
        rateMaps2 = self.create_rateMaps()
        rateMaps2[:, :self.n_mec] = self.rateMaps[:, :self.n_mec]

        new_rateMaps = np.zeros_like(self.rateMaps)

        idx = np.arange(self.rateMaps.shape[0])
        np.random.shuffle(idx)

        new_rateMaps[idx[int(idx.size * dd):]] = self.rateMaps[idx[int(idx.size * dd):]]
        new_rateMaps[idx[:int(idx.size * dd)]] = rateMaps2[idx[:int(idx.size * dd)]]
        
        if permanent:
            self.rateMaps = new_rateMaps
        
        return new_rateMaps
    
    
    def get_rateMaps(self):
        
        return self.rateMaps
    
    
    def plot_rateMaps(self):
        '''
        Plots a subset of LEC and MEC ratemaps as an example.
        '''
        
        print('MEC maps')
        plt.figure(figsize=(15,5))
        for i, cell_n in enumerate(np.random.randint(0, self.n_mec,  5 )):
            plt.subplot(1,5,i+1)
            plt.imshow(self.rateMaps[:,cell_n].reshape(self.dims))
            plt.axis('off')
        plt.show()
            
        print('')
        print('LEC maps')
        plt.figure(figsize=(15,5))
        for i, cell_n in enumerate(np.random.randint(self.n_mec, self.n_mec+self.n_lec, 5 )):
            plt.subplot(1,5,i+1)
            plt.imshow(self.rateMaps[:,cell_n].reshape(self.dims))
            plt.axis('off')
        plt.show()
