import numpy as np
from scipy import stats


def ratemaps_all_layers(model, data, arena_size, n_DG, n_CA3, n_CA1):

    layers = ['DG', 'CA3', 'CA1']
    n_layer = [n_DG, n_CA3, n_CA1]
    ratemaps = []
    for indx, l in enumerate(layers):
        r = model.get_output(layer=l, data=data).reshape((arena_size[0], arena_size[1], n_layer[indx]))
        ratemaps_shaped = np.array([r[:,:,i] for i in np.arange(n_layer[indx])])
        ratemaps.append(ratemaps_shaped)
        
    return ratemaps


def get_number_of_place_fields(ratemap):
    
    gauss_kernel_sigma      = 9  # px
    active_pixels_threshold = .2 # 20 percent
    cluster_min             = 200/2
    cluster_max             = 2500/2
    
    ratemap[ ratemap <  ratemap.max()*active_pixels_threshold ] = 0
    ratemap[ ratemap >= ratemap.max()*active_pixels_threshold ] = 1


    visited_matrix  = np.zeros_like(ratemap)


    ratemap[ ratemap <  ratemap.max()*active_pixels_threshold ] = 0
    ratemap[ ratemap >= ratemap.max()*active_pixels_threshold ] = 1

    visited_matrix  = np.zeros_like(ratemap)

    
    ## First pass of clustering
    clusterd_matrix = np.zeros_like(ratemap)
    current_cluster = 1

    #go through every bin in the ratemap
    for yy in range(1,ratemap.shape[0]-1):
        for xx in range(1,ratemap.shape[1]-1):

            if ratemap[  yy, xx ] == 1:

                ## go through every bin around this bin
                for ty in range(-1,2):
                    for tx in range(-1,2):

                        if clusterd_matrix[ yy+ty, xx+tx ] != 0:
                            clusterd_matrix[ yy,xx ] = clusterd_matrix[ yy+ty, xx+tx ]

                if clusterd_matrix[ yy, xx ] == 0:
                    current_cluster += 1
                    clusterd_matrix[ yy, xx ] = current_cluster

                    
    ## Refine clustering: neighbour bins to same cluster number
    for yy in range(1,clusterd_matrix.shape[0]-1):
        for xx in range(1,clusterd_matrix.shape[1]-1):


            if clusterd_matrix[  yy, xx ] != 0:

                ## go through every bin around this bin
                for ty in range(-1,2):
                    for tx in range(-1,2):

                        if clusterd_matrix[ yy+ty, xx+tx ] != 0:
                            if clusterd_matrix[ yy+ty, xx+tx ] != clusterd_matrix[  yy, xx ]:
                                clusterd_matrix[ yy+ty, xx+tx ] = clusterd_matrix[  yy, xx ]

                                
    ## Quantify number of place fields
    clusters_labels = np.delete(np.unique(clusterd_matrix),   np.where(  np.unique(clusterd_matrix) ==0 ) )

    n_place_fields_counter = 0

    for k in range(clusters_labels.size):

        n_bins = np.where(clusterd_matrix == clusters_labels[k] )[0].size

        if cluster_min <= n_bins <= cluster_max:

            n_place_fields_counter += 1

                 
    return n_place_fields_counter


def placeFields_distr(model, data, arena_size, n_DG, n_CA3, n_CA1):

        # DG
        tmp_rateMaps_DG = model.get_output(layer='DG', data=data).reshape((arena_size[0], arena_size[1], n_DG))
        rateMaps_DG = np.array([tmp_rateMaps_DG[:,:,i] for i in np.arange(n_DG)])
        n_placeFields_DG = []
        for neuron in np.arange(n_DG):
            n = get_number_of_place_fields(rateMaps_DG[neuron])
            n_placeFields_DG.append(n)
        placeFields_distr_DG, _ = np.histogram(n_placeFields_DG, bins=6, range=(0,6), density=True)

        # CA3
        tmp_rateMaps_CA3 = model.get_output(layer='CA3', data=data).reshape((arena_size[0], arena_size[1], n_CA3))
        rateMaps_CA3 = np.array([tmp_rateMaps_CA3[:,:,i] for i in np.arange(n_CA3)])
        n_placeFields_CA3 = []
        for neuron in np.arange(n_CA3):
            n = get_number_of_place_fields(rateMaps_CA3[neuron])
            n_placeFields_CA3.append(n)
        placeFields_distr_CA3, _ = np.histogram(n_placeFields_CA3, bins=6, range=(0,6), density=True)

        # CA1
        tmp_rateMaps_CA1 = model.get_output(layer='CA1', data=data).reshape((arena_size[0], arena_size[1], n_CA1))
        rateMaps_CA1 = np.array([tmp_rateMaps_CA1[:,:,i] for i in np.arange(n_CA1)])
        n_placeFields_CA1 = []
        for neuron in np.arange(n_CA1):
            n = get_number_of_place_fields(rateMaps_CA1[neuron])
            n_placeFields_CA1.append(n)
        placeFields_distr_CA1, _ = np.histogram(n_placeFields_CA1, bins=6, range=(0,6), density=True)

        placeFields_distr = np.array([placeFields_distr_DG, placeFields_distr_CA3, placeFields_distr_CA1])

        return placeFields_distr
    
    
def rateRemapping(model, arena, data, layer='DG'):

    rateMaps_DG = model.get_output(layer=layer, data=data)

    pv_corr = []
    lec_corr = []
    for dd in np.linspace(0, 1, 7):

        modif_data = arena.modify_LEC_maps(dd=dd)

        new_rateMaps_DG = model.get_output(layer=layer, data=modif_data)

        pv_corr.append( stats.pearsonr(rateMaps_DG.flatten(), new_rateMaps_DG.flatten())[0] )
        lec_corr.append( stats.pearsonr(data.flatten(), modif_data.flatten())[0] )

    pv_corr = np.array(pv_corr)
    lec_corr = np.array(lec_corr)

    return pv_corr, pv_corr*lec_corr


def interpolate_LEC_maps(rateMaps1, rateMaps2, dd):
    '''
    Params:
        ratemaps1 (numpy array) : The ratemaps of the original arena.
        rateMaps2 (numpy array) : The new ratemaps to be interpolated with rateMaps1. It should be a function of Arena.modify_LEC_maps() to preserve same grid cells across maps.
        dd (float) : Defines the proportion (ratio) of LEC-rateMaps2/LEC-rateMaps1 that will be selected for new rateMap.
    '''

    new_rateMaps = np.zeros_like(rateMaps1)

    idx = np.arange(rateMaps1.shape[0])
    np.random.shuffle(idx)

    new_rateMaps[idx[int(idx.size * dd):]] = rateMaps1[idx[int(idx.size * dd):]]
    new_rateMaps[idx[:int(idx.size * dd)]] = rateMaps2[idx[:int(idx.size * dd)]]

    return new_rateMaps
